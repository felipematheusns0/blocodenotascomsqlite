package br.ifsc.edu.blocodenotassqlite;

import android.content.Context;

public class NotasController {
    NotasDAO notasDAO;
    Context context;
    public NotasController(Context c) {
        this.context = c;
        notasDAO = new NotasDAO(context);
    }

    public void adicionaTexto(int id,String s) {
        notasDAO.inserir(id, s);
    }

    public String retornaTexto() {
        return notasDAO.retorna();
    }
    public boolean teveRetorno(){
        if (notasDAO.retornaTamanho() > 0){
            return true;
        }else{
            return false;
        }
    }

    public boolean verificaSeHaId(int i) {
        if(notasDAO.verificaId(i) > 0){
            return true;
        }else {
            return false;
        }
    }

    public void atualizaTexto(int i, String s) {
        notasDAO.attTexto(i, s);
    }
}
