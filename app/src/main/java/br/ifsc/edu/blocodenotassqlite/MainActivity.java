package br.ifsc.edu.blocodenotassqlite;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText editText;
    TextView textView;
    NotasController notasController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = findViewById(R.id.ediText);
        textView = findViewById(R.id.textoRecuperado);
        notasController = new NotasController(getApplicationContext());


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editText.getText().toString().trim().equals("")){
                    Toast.makeText(getApplicationContext(), "NÃO É POSSÍVEL ADICIONAR UM VALOR VAZIO", Toast.LENGTH_LONG);
                }else{
                    if(notasController.verificaSeHaId(1)){
                        notasController.atualizaTexto(1, editText.getText().toString());
                    }else{
                        notasController.adicionaTexto(1, editText.getText().toString());
                    }
                    textView.setText(editText.getText().toString());
                }

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (notasController.teveRetorno()){
            String a = notasController.retornaTexto();
            textView.setText(notasController.retornaTexto());
        }else{
            Toast.makeText(getApplicationContext(), "NÃO HÁ TEXTO PARA RETORNAR", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(notasController.verificaSeHaId(1)){
            notasController.atualizaTexto(1, editText.getText().toString());
        }else{
            notasController.adicionaTexto(1, editText.getText().toString());
        }
    }
}
