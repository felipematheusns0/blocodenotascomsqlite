package br.ifsc.edu.blocodenotassqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class NotasDAO {
    SQLiteDatabase db;
    public NotasDAO(Context c) {

        db=c.openOrCreateDatabase("NotasDB", c.MODE_PRIVATE, null);

        db.execSQL("CREATE TABLE IF NOT EXISTS notas (" +
                "id INTEGER NOT NULL,"+
                "texto VARCHAR NOT NULL);");

    }

    public void inserir(int id, String s) {
        String sql = "INSERT INTO notas (id, texto) VALUES ("+id+",'"+s+"');";
        db.execSQL(sql);
    }

    public String retorna() {
        Cursor cursor;
        cursor = db.rawQuery("SELECT texto FROM notas", null);
        cursor.moveToFirst();
        String valor = cursor.getString(cursor.getColumnIndex("texto"));
        return valor;
    }

    public int retornaTamanho(){
        Cursor cursor;
        cursor = db.rawQuery("SELECT * FROM notas", null);
        cursor.moveToFirst();
        int resultado = cursor.getCount();
        return resultado;
    }

    public int verificaId(int i) {
        Cursor cursor;
        cursor = db.rawQuery("SELECT * FROM notas WHERE id = "+i+";", null);
        int retorno = cursor.getCount();
        return retorno;
    }

    public void attTexto(int i, String s) {
        db.execSQL("UPDATE notas SET texto = '"+s+"' WHERE id = "+i+";");
    }
}
